#### 0.0.0 (2024-01-24)

##### Breaking Changes

*  alters ([a059c7e1](git+https://gitlab.com/rackspace7415231/my-changelog-backstage-demo/commit/a059c7e1d7c9704bb88a0d52d67067a98a519301))
*  alters ([fc6aeb69](git+https://gitlab.com/rackspace7415231/my-changelog-backstage-demo/commit/fc6aeb690bb5d013f4d62c997fe166fd69350a29))

##### Bug Fixes

* adding config.yaml ([5c8545f5](git+https://gitlab.com/rackspace7415231/my-changelog-backstage-demo/commit/5c8545f57a6e148fb442008df60085ab906969ce))

## 1.0.0 (2024-01-24)

##### Breaking Changes

*  alters ([fc6aeb69](git+https://gitlab.com/rackspace7415231/my-changelog-backstage-demo/commit/fc6aeb690bb5d013f4d62c997fe166fd69350a29))

## 1.0.0 (2024-01-24)

#### 0.0.0 (2024-01-24)

#### 1.0.0 (2024-01-24)

#### 1.0.0 (2024-01-24)
major
#### 1.0.0 (2024-01-24)

